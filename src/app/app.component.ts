import { Component } from '@angular/core';
import { Platform, Gesture } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { NovaPaginaPage } from '../pages/nova-pagina/nova-pagina';
import { ListaTarefasPage } from '../pages/lista-tarefas/lista-tarefas';
import { ListaDoisPage } from '../pages/lista-dois/lista-dois';
import { ExercicioUmL2Page } from '../pages/exercicio-um-l2/exercicio-um-l2';
import { BindPage } from '../pages/bind/bind';
import { CincoCaracterPage } from '../pages/cinco-caracter/cinco-caracter';
import { InsertImagePage } from '../pages/insert-image/insert-image';
import { HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FirebaseProvider } from '../providers/firebase/firebase';
import { L2E4CardPage } from '../pages/l2-e4-card/l2-e4-card';
import { L3E1DataAtivPage } from '../pages/l3-e1-data-ativ/l3-e1-data-ativ';
import { Facebook } from '@ionic-native/facebook';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
 // rootPage:any = HomePage;
 //rootPage:any = NovaPaginaPage;
 //rootPage:any = ListaTarefasPage;
 //rootPage: any = ListaDoisPage;
 //rootPage: any = ExercicioUmL2Page;
 //rootPage: any = BindPage;
 //rootPage: any = CincoCaracterPage;
  //rootPage: any = SideMenuPage;
  //rootPage: any = ExercicioUmL2Page;
  //rootPage: any = L3E1DataAtivPage;
  rootPage: any = "TodosExPage";
  
  constructor(public firebase: FirebaseProvider, 
    platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.firebase.auth().onAuthStateChanged(
      user => {
        if(!user){
            this.rootPage="TodosExPage";
        }else{
            this.rootPage = "TodosExPage";
        }
      }
    );
  }
}

