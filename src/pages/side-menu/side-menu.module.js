var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { IonicPageModule, IonicPage } from 'ionic-angular';
import { SideMenuPage } from './side-menu';
import { HomePage } from '../home/home';
import { NovaPaginaPage } from '../nova-pagina/nova-pagina';
import { ListaTarefasPage } from '../lista-tarefas/lista-tarefas';
var SideMenuPageModule = /** @class */ (function () {
    function SideMenuPageModule() {
    }
    SideMenuPageModule = __decorate([
        NgModule({
            declarations: [
                SideMenuPage,
                HomePage,
                NovaPaginaPage,
                ListaTarefasPage,
            ],
            imports: [
                IonicPageModule.forChild(SideMenuPage),
                IonicPage.call(ListaTarefasPage),
            ],
        })
    ], SideMenuPageModule);
    return SideMenuPageModule;
}());
export { SideMenuPageModule };
//# sourceMappingURL=side-menu.module.js.map