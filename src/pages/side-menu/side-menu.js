var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { App, ViewController } from 'ionic-angular';
import { ListaTarefasPage } from '../lista-tarefas/lista-tarefas';
/**
 * Generated class for the SideMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SideMenuPage = /** @class */ (function () {
    function SideMenuPage(navCtrl, viewCtrl, appCtrl) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.appCtrl = appCtrl;
    }
    SideMenuPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SideMenuPage');
    };
    SideMenuPage.prototype.openNav = function () {
        document.getElementById("mySidenav").style.width = "200px";
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    };
    /* Set the width of the side navigation to 0 */
    SideMenuPage.prototype.closeNav = function () {
        document.getElementById("mySidenav").style.width = "0";
        document.body.style.backgroundColor = "white";
    };
    SideMenuPage.prototype.openHome = function () {
        this.appCtrl.getRootNav().setRoot(ListaTarefasPage);
    };
    SideMenuPage.prototype.closeHome = function () {
        this.appCtrl.goBack();
    };
    SideMenuPage = __decorate([
        IonicPage(),
        Component({
            templateUrl: 'side-menu.html',
        }),
        __metadata("design:paramtypes", [NavController, ViewController, App])
    ], SideMenuPage);
    return SideMenuPage;
}());
export { SideMenuPage };
//# sourceMappingURL=side-menu.js.map