import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { App, ViewController } from 'ionic-angular';
import { ListaTarefasPage } from '../lista-tarefas/lista-tarefas';

/**
 * Generated class for the SideMenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  templateUrl:'side-menu.html',
})
export class SideMenuPage {
  
  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public appCtrl: App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SideMenuPage');
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "200px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
  }

/* Set the width of the side navigation to 0 */
  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.body.style.backgroundColor = "white";
  }

  openHome(){
   this.appCtrl.getRootNav().setRoot(ListaTarefasPage);
  }

  closeHome(){
    this.appCtrl.goBack();
  }

}