import { NgModule } from '@angular/core';
import { IonicPageModule, IonicPage } from 'ionic-angular';
import { SideMenuPage } from './side-menu';
import { HomePage } from '../home/home';
import { NovaPaginaPage } from '../nova-pagina/nova-pagina';
import { ListaTarefasPage } from '../lista-tarefas/lista-tarefas';

@NgModule({
  declarations: [
    SideMenuPage,
    HomePage,
    NovaPaginaPage,
    ListaTarefasPage,
  ],
  imports: [
    IonicPageModule.forChild(SideMenuPage),
    IonicPage.call(ListaTarefasPage),
  ],
})
export class SideMenuPageModule {}
