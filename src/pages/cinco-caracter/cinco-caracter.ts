import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CincoCaracterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cinco-caracter',
  templateUrl: 'cinco-caracter.html',
})
export class CincoCaracterPage {

  public label = 'Informe 5 caracteres!'
  public textInput = '';
  public teste;

  changeLabel(){
    if(this.textInput.length == 5){
      this.label = '';
      this.teste = true;
    }
    if(this.textInput.length == 0){
      this.label = 'Informe 5 caracteres!'
    }
  }

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CincoCaracterPage');
  }
  back(){
    this.navCtrl.pop();
  }

}
