import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CincoCaracterPage } from './cinco-caracter';

@NgModule({
  declarations: [
    CincoCaracterPage,
  ],
  imports: [
    IonicPageModule.forChild(CincoCaracterPage),
  ],
})
export class CincoCaracterPageModule {}
