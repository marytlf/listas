import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginCameraPage } from './login-camera';

@NgModule({
  declarations: [
    LoginCameraPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginCameraPage),
  ],
})
export class LoginCameraPageModule {}
