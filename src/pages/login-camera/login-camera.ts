import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';

/**
 * Generated class for the LoginCameraPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login-camera',
  templateUrl: 'login-camera.html',
})
export class LoginCameraPage {

    public foto = '';
    public imgRet = '';

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public camera: Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginCameraPage');
  }

  async tirarFoto(){

        let opcoes ={
            quality:95,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };

        let captura = await this.camera.getPicture(opcoes);
        this.foto = 'data:image/jpeg;base64,' + captura;


  }


}
