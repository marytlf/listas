import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the L2E4CardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-l2-e4-card',
  templateUrl: 'l2-e4-card.html',
})
export class L2E4CardPage {

    public avatar = "assets/imgs/1.jpg"

    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

    ionViewDidLoad() {
    console.log('ionViewDidLoad L2E4CardPage');
    }

    back(){
        this.navCtrl.pop();
    }
    

}
