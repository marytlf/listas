import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { L2E4CardPage } from './l2-e4-card';

@NgModule({
  declarations: [
    L2E4CardPage,
  ],
  imports: [
    IonicPageModule.forChild(L2E4CardPage),
  ],
})
export class L2E4CardPageModule {}
