var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the L2E4CardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var L2E4CardPage = /** @class */ (function () {
    function L2E4CardPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.avatar = "assets/imgs/1.jpg";
    }
    L2E4CardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad L2E4CardPage');
    };
    L2E4CardPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    L2E4CardPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-l2-e4-card',
            templateUrl: 'l2-e4-card.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], L2E4CardPage);
    return L2E4CardPage;
}());
export { L2E4CardPage };
//# sourceMappingURL=l2-e4-card.js.map