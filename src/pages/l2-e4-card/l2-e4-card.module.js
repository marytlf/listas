var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { L2E4CardPage } from './l2-e4-card';
var L2E4CardPageModule = /** @class */ (function () {
    function L2E4CardPageModule() {
    }
    L2E4CardPageModule = __decorate([
        NgModule({
            declarations: [
                L2E4CardPage,
            ],
            imports: [
                IonicPageModule.forChild(L2E4CardPage),
            ],
        })
    ], L2E4CardPageModule);
    return L2E4CardPageModule;
}());
export { L2E4CardPageModule };
//# sourceMappingURL=l2-e4-card.module.js.map