import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, RootNode } from 'ionic-angular';
import { SideMenuPage } from '../side-menu/side-menu';


/**
 * Generated class for the ListaTarefasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage(
)
@Component({
  selector: 'page-lista-tarefas',
  templateUrl: 'lista-tarefas.html',
})
export class ListaTarefasPage {

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListaTarefasPage');
  }

  back(){
    this.navCtrl.pop();
  }

}
