var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { UsuarioProvider } from '../../providers/usuario/usuario';
/**
 * Generated class for the L3E1DataAtivPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var L3E1DataAtivPage = /** @class */ (function () {
    function L3E1DataAtivPage(navCtrl, navParams, modalCtrl, toastCtrl, alertCtrl, firebase, usuario) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.firebase = firebase;
        this.usuario = usuario;
        this.nomeReturn = "";
        this.atividades = [];
        this.ativRet = [];
        this.tipo = false; //false=trabalho, true=atividade
        this.ativ = {
            id: '',
            conteudo: '',
            nome: '',
            tipo: ''
        };
        this.listAtividades();
    }
    L3E1DataAtivPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad L3E1DataAtivPage');
    };
    L3E1DataAtivPage.prototype.listAtividades = function () {
        return __awaiter(this, void 0, void 0, function () {
            var results, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.firebase.db().collection("atividades").get()];
                    case 1:
                        results = _a.sent();
                        results.docs.forEach(function (doc) {
                            _this.atividades.push(__assign({ id: doc.id }, doc.data()));
                        });
                        console.log(this.atividades);
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        throw new Error(e_1);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    L3E1DataAtivPage.prototype.openModal = function (atividade) {
        console.log("ativ data = " + atividade.data);
        var janela = this.modalCtrl.create("L3E1AtivModalPage", { name: atividade.name, conteudo: atividade.conteudo, data: atividade.data, tipo: atividade.tipo, id: atividade.id });
        janela.onDidDismiss(function (dataRet) {
            atividade.data = dataRet.data;
            atividade.name = dataRet.name;
            atividade.conteudo = dataRet.conteudo;
            atividade.tipo = dataRet.tipo;
        });
        janela.present();
    };
    L3E1DataAtivPage.prototype.openModalSave = function () {
        var _this = this;
        var janela = this.modalCtrl.create("L3E1AtivModalPage", { update: true });
        janela.onDidDismiss(function (dataRet) {
            _this.data = dataRet.data;
            _this.nome = dataRet.nome;
            _this.conteudo = dataRet.conteudo;
            _this.tipo = dataRet.tipo;
            _this.id = dataRet.id;
        });
        janela.present();
    };
    L3E1DataAtivPage.prototype.back = function () {
        this.navCtrl.setRoot("TodosExPage");
    };
    L3E1DataAtivPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-l3-e1-data-ativ',
            templateUrl: 'l3-e1-data-ativ.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            ModalController,
            ToastController,
            AlertController,
            FirebaseProvider,
            UsuarioProvider])
    ], L3E1DataAtivPage);
    return L3E1DataAtivPage;
}());
export { L3E1DataAtivPage };
//# sourceMappingURL=l3-e1-data-ativ.js.map