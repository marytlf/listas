import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { UsuarioProvider } from '../../providers/usuario/usuario';

/**
 * Generated class for the L3E1DataAtivPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-l3-e1-data-ativ',
  templateUrl: 'l3-e1-data-ativ.html',
})
export class L3E1DataAtivPage {

    public myDate;
    public atividadesRet;
    public nome;
    public conteudo;
    public id:any;
    public data;
    public nomeReturn="";
    public conteudoReturn;
    public dataReturn;
    public concluido:boolean=false;
    public atividades = [];
    public ativRet=[];
    public tipo:boolean = false; //false=trabalho, true=atividade
    public ativ={
        id:'',
        conteudo:'',
        nome:'',
        tipo:''
    }


  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public firebase: FirebaseProvider,
    public usuario: UsuarioProvider,
    ) { 
       this.listAtividades();
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad L3E1DataAtivPage')
  }

  async listAtividades(){
      try{
        let results = await this.firebase.db().collection("atividades");
        let query = results.where('user_id','==',this.usuario.get().uid).get().then(
            snapshot=>{
                snapshot.forEach( doc =>{
                    this.atividades.push({id:doc.id,...doc.data()});
            });
           
        });

        console.log(this.atividades)
      }catch(e){
        throw new Error(e);
      }
   
  }

    openModal(atividade){
        console.log("ativ data = "+atividade.data)
    let janela = this.modalCtrl.create("L3E1AtivModalPage", {name: atividade.name, conteudo:atividade.conteudo, data:atividade.data, tipo:atividade.tipo, id:atividade.id, concluido:atividade.concluido});
    
    janela.onDidDismiss(dataRet=>{
        atividade.data = dataRet.data;
        atividade.name=dataRet.name
        atividade.conteudo=dataRet.conteudo
        atividade.tipo=dataRet.tipo
        atividade.concluido = dataRet.concluido
    })
    janela.present();
  }

    openModalSave(){
        let janela = this.modalCtrl.create("L3E1AtivModalPage", {update:true});
        janela.onDidDismiss(dataRet=>{
            
            this.data=dataRet.data
            this.nome=dataRet.nome
            this.conteudo=dataRet.conteudo
            this.tipo=dataRet.tipo
            this.id=dataRet.id
            this.concluido = dataRet.concluido
        })
        janela.present();
    }
    back(){
        this.navCtrl.setRoot("TodosExPage")
    }
    

}
