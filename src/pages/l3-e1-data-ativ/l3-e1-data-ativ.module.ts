import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { L3E1DataAtivPage } from './l3-e1-data-ativ';

@NgModule({
  declarations: [
    L3E1DataAtivPage,
  ],
  imports: [
    IonicPageModule.forChild(L3E1DataAtivPage),
  ],
})
export class L3E1DataAtivPageModule {}
