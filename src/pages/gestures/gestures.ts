import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the GesturesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gestures',
  templateUrl: 'gestures.html',
})
export class GesturesPage {

  public numero = 0;
  public senha;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public alert: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GesturesPage');
  }

  metodo(evento){
    console.log(evento);
    this.numero++;
  }

  abrirAlerta(){
    let janela =  this.alert.create({
      title: "Boa noite",
      subTitle: "Como vai você?",
      buttons: ["Não muito bem","Cansada"]
    });
    janela.present();
  }

  public novoAlerta(){
    let janela = this.alert.create();
    janela.setTitle("Informe a senha!");
    janela.addInput({
      type: 'text',
      placeholder: 'Sua Senha',
      name: 'senha'
    });
    janela.addButton({
      text: 'Salvar',
      handler: (form) => this.senha = form.senha
    });
    janela.present();
  }
}
