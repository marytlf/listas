var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
/**
 * Generated class for the GesturesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GesturesPage = /** @class */ (function () {
    function GesturesPage(navCtrl, navParams, alert) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alert = alert;
        this.numero = 0;
    }
    GesturesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad GesturesPage');
    };
    GesturesPage.prototype.metodo = function (evento) {
        console.log(evento);
        this.numero++;
    };
    GesturesPage.prototype.abrirAlerta = function () {
        var janela = this.alert.create({
            title: "Boa noite",
            subTitle: "Como vai você?",
            buttons: ["Não muito bem", "Cansada"]
        });
        janela.present();
    };
    GesturesPage.prototype.novoAlerta = function () {
        var _this = this;
        var janela = this.alert.create();
        janela.setTitle("Informe a senha!");
        janela.addInput({
            type: 'text',
            placeholder: 'Sua Senha',
            name: 'senha'
        });
        janela.addButton({
            text: 'Salvar',
            handler: function (form) { return _this.senha = form.senha; }
        });
        janela.present();
    };
    GesturesPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-gestures',
            templateUrl: 'gestures.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            AlertController])
    ], GesturesPage);
    return GesturesPage;
}());
export { GesturesPage };
//# sourceMappingURL=gestures.js.map