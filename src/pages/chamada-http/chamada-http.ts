import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';

/**
 * Generated class for the ChamadaHttpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chamada-http',
  templateUrl: 'chamada-http.html',
})
export class ChamadaHttpPage {

    public categorias = [];
    public tags = [];
    public noticias = [];
    public search = '';

  constructor(public navCtrl: NavController, 
                public navParams: NavParams,
                public http: Http,
                public modalCtrl: ModalController,
                public toastCtrl: ToastController,
                public alertCtrl: AlertController,) {
             this.metodoGET();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChamadaHttpPage');
  }


  metodoGET(){

      let url = 'http://content.guardianapis.com/search?order-by=newest&show-fields=all&page-size=50&api-key=test';
      let header = {};
     

      this.http.get(url, header).subscribe(
          sucesso=>{
              let news = sucesso.json().response.results;

              for ( let i = 0; i < news.length; i++){
                console.log(news[i])
                  this.noticias.push({
                        titulo: news[i].webTitle,
                        categoria: news[i].sectionName,
                        url: news[i].webUrl,
                        conteudo: news[i].fields.bodyText
                  });
                  
              }
          }
      )
  }

    metodoGETCategory(){

    let url = 'https://content.guardianapis.com/sections?&api-key=test';
    let header = {};

    this.http.get(url, header).subscribe(
        sucesso=>{
            let category = sucesso.json().response.results;

            for ( let i = 0; i < category.length; i++){
                console.log(category[i])
                this.categorias.push({
                        titulo: category[i].webTitle,
                        url: category[i].webUrl
                });
                
            }
        }
    )
    }

    metodoGETTags(){

        let url = 'https://content.guardianapis.com/tags?&page-size=50&api-key=test';
        let header = {};
       
        this.http.get(url, header).subscribe(
            sucesso=>{
                let tags = sucesso.json().response.results;
    
                for ( let i = 0; i < tags.length; i++){
                  console.log(tags[i])
                    this.tags.push({
                          tag: tags[i].id,
                          url: tags[i].webUrl
                    });
                    
                }
            }
        )
    }
    




    showContent(objNews){
        console.log(objNews.titulo)
        let janela = this.modalCtrl.create("BodyContentPage", {titulo: objNews.titulo, conteudo:objNews.conteudo, categoria: objNews.categoria });
        janela.present();
    }

    onInput(event: any){
        console.log(event)
    }

    back(){
        this.navCtrl.pop()
    }
}
