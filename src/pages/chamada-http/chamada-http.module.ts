import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChamadaHttpPage } from './chamada-http';

@NgModule({
  declarations: [
    ChamadaHttpPage,
  ],
  imports: [
    IonicPageModule.forChild(ChamadaHttpPage),
  ],
})
export class ChamadaHttpPageModule {}
