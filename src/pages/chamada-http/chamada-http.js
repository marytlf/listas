var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
/**
 * Generated class for the ChamadaHttpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChamadaHttpPage = /** @class */ (function () {
    function ChamadaHttpPage(navCtrl, navParams, http, modalCtrl, toastCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.categorias = [];
        this.tags = [];
        this.noticias = [];
        this.search = '';
        this.metodoGET();
    }
    ChamadaHttpPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChamadaHttpPage');
    };
    ChamadaHttpPage.prototype.metodoGET = function () {
        var _this = this;
        var url = 'http://content.guardianapis.com/search?order-by=newest&show-fields=all&page-size=200&api-key=test';
        var header = {};
        this.http.get(url, header).subscribe(function (sucesso) {
            var news = sucesso.json().response.results;
            for (var i = 0; i < news.length; i++) {
                console.log(news[i]);
                _this.noticias.push({
                    titulo: news[i].webTitle,
                    categoria: news[i].sectionName,
                    url: news[i].webUrl,
                    conteudo: news[i].fields.bodyText
                });
            }
        });
    };
    ChamadaHttpPage.prototype.metodoGETCategory = function () {
        var _this = this;
        var url = 'https://content.guardianapis.com/sections?&api-key=test';
        var header = {};
        this.http.get(url, header).subscribe(function (sucesso) {
            var category = sucesso.json().response.results;
            for (var i = 0; i < category.length; i++) {
                console.log(category[i]);
                _this.categorias.push({
                    titulo: category[i].webTitle,
                    url: category[i].webUrl
                });
            }
        });
    };
    ChamadaHttpPage.prototype.metodoGETTags = function () {
        var _this = this;
        var url = 'https://content.guardianapis.com/tags?&page-size=200&api-key=test';
        var header = {};
        this.http.get(url, header).subscribe(function (sucesso) {
            var tags = sucesso.json().response.results;
            for (var i = 0; i < tags.length; i++) {
                console.log(tags[i]);
                _this.tags.push({
                    tag: tags[i].id,
                    url: tags[i].webUrl
                });
            }
        });
    };
    ChamadaHttpPage.prototype.showContent = function (objNews) {
        console.log(objNews.titulo);
        var janela = this.modalCtrl.create("BodyContentPage", { titulo: objNews.titulo, conteudo: objNews.conteudo, categoria: objNews.categoria });
        janela.present();
    };
    ChamadaHttpPage.prototype.onInput = function (event) {
        console.log(event);
    };
    ChamadaHttpPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    ChamadaHttpPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-chamada-http',
            templateUrl: 'chamada-http.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            Http,
            ModalController,
            ToastController,
            AlertController])
    ], ChamadaHttpPage);
    return ChamadaHttpPage;
}());
export { ChamadaHttpPage };
//# sourceMappingURL=chamada-http.js.map