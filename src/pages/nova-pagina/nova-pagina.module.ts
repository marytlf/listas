import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NovaPaginaPage } from './nova-pagina';
import { ListaDoisPage } from '../lista-dois/lista-dois';


@NgModule({
  declarations: [
    NovaPaginaPage,
    ListaDoisPage,
  ],
  imports: [
    IonicPageModule.forChild(NovaPaginaPage),
  ],
})
export class NovaPaginaPageModule {}
