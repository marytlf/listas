import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ListaDoisPage } from '../lista-dois/lista-dois';
import { FirebaseProvider } from '../../providers/firebase/firebase'
import { Facebook } from '@ionic-native/facebook';
import { HttpModule, Http } from '@angular/http';

/**
 * Generated class for the NovaPaginaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nova-pagina',
  templateUrl: 'nova-pagina.html',
})
export class NovaPaginaPage {

  public pushPage: any;
  public email: string = '';
  public password: any = '';
  public usuario = {logado:'false'};


  constructor(public alert: AlertController,
            public firebase: FirebaseProvider,
            public facebook: Facebook,
            public http: Http,
            public navCtrl: NavController) {
            this.checkStatus();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NovaPaginaPage');
  }


  async checkStatus(){
      let status = await this.facebook.getLoginStatus();
      if(status.status === 'connected'){
          await this.dadosUsuario();
          
      }else{
          await this.loginFb();
      }
  }

  async login(){
    try{
      await this.firebase.auth().signInWithEmailAndPassword(this.email, this.password);
      this.abrirAlerta();
      this.open("TodosExPage");
    }catch(e){
      throw new Error(e);
    }
  }

  async criarConta(){
    try{
      await this.firebase.auth().createUserWithEmailAndPassword(this.email, this.password);
    }catch(e){
      throw new Error(e);
    }
  }

  abrirAlerta(){
    let janela = this.alert.create({
      title: "Prog. Móvel",
      subTitle: "Você deve fazer as listas!",
      buttons: ["Ok!"]
    });
    janela.present();
  }

  async dadosUsuario(){
      let dados = await this.facebook.api('/me?fields=picture.width(100).height(100), name',['public_profile']);
      this.usuario['foto'] = dados.picture.data.url;
      this.usuario['nome'] = dados.name;
      this.usuario['logado'] = 'conectado';
      
  }

  async sair(){
      await this.facebook.logout();
      this.usuario.logado = 'false';
  }


  async loginFb(){
      let permissions = ['public_profile','email'];

      let response = await this.facebook.login(permissions);
      this.usuario['token'] = response.authResponse.accessToken;
      this.usuario['id'] = response.authResponse.userID;
      this.usuario['status'] = response.status;

      await this.dadosUsuario();
      this.open("TodosExPage")

  }


    open(paginaPar){
        this.navCtrl.push(paginaPar);
    }
    
  back(){
    this.navCtrl.pop();
  }

}
