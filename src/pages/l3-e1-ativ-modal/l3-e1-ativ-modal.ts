import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController, ViewController, Button } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { UsuarioProvider } from '../../providers/usuario/usuario';

/**
 * Generated class for the L3E1AtivModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-l3-e1-ativ-modal',
  templateUrl: 'l3-e1-ativ-modal.html',
})
export class L3E1AtivModalPage {
    public name='';
    public conteudo ='';
    public data;
    public tipo:boolean=false;
    public answer:any;
    public ativ = [];
    public update:boolean=false;
    public id;
    public myDate: String = new Date().toISOString();
    public date = new Date().toDateString();
    public concluido:boolean = false;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public view: ViewController,
    public firebase: FirebaseProvider,
    public usuario: UsuarioProvider,
    public alert: AlertController){

    this.name = this.navParams.get("name");
    this.conteudo  = this.navParams.get('conteudo')
    this.data = this.navParams.get('data')
    this.tipo = this.navParams.get('tipo')
    this.update = this.navParams.get('update')
    this.id = this.navParams.get('id')
    this.concluido = this.navParams.get('concluido')

    console.log('aaaaaa'+this.id)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad L3E1AtivModalPage');
  }


  async saveAtiv(){
    try{
       
        await this.firebase.db().collection("atividades").add({
            name: this.name,
            data: this.data,
            conteudo: this.conteudo,
            tipo: this.tipo,
            user_id: this.usuario.get().uid,
            concluido: this.concluido,
        });
        let janela = this.toastCtrl.create({
            message: "Atividade salva com sucesso!",
            duration:2000
        });
        janela.present();
        this.fechar();
    }catch(e){
      let janela = this.toastCtrl.create({
        message: "Opa! Um erro foi detectado!",
        duration:2000
      });
      janela.present();

      throw new Error(e);
    }
  }


  async updateAtiv(){
    try{ 
       
        if(this.data == null){
            
            this.data = this.myDate
        }
        if(this.tipo == false || this.tipo == null){
            this.tipo = false;
        }
        
        await this.firebase.db().collection("atividades").doc(this.id).update({
            name: this.name,
            data: this.data,
            conteudo: this.conteudo,
            tipo: this.tipo,
            user_id: this.usuario.get().uid,
            concluido: this.concluido,
        });
       
        let janela = this.toastCtrl.create({
        message: "Atividade atualizada com sucesso!",
        duration:2000
      });
      janela.present();
      let dataReturn = {name:this.name,conteudo:this.conteudo,data:this.data, tipo:this.tipo}
      this.view.dismiss(dataReturn);
      this.update=false;
    }catch(e){
      let janela = this.toastCtrl.create({
        message: "Opa! Um erro foi detectado!",
        duration:2000
      });
      janela.present();

      throw new Error(e);
    }
  }

  async deleteAtiv(){
    try{ 
        await this.firebase.db().collection("atividades").doc(this.id).delete();
        let janela = this.toastCtrl.create({
        message: "Atividade excluída com sucesso!",
        duration:2000
      });
      janela.present();
      this.fechar();
    }catch(e){
      let janela = this.toastCtrl.create({
        message: "Opa! Um erro foi detectado!",
        duration:2000
      });
      janela.present();

      throw new Error(e);
    }
  }


  presentAlert() {
    
    let alert = this.alertCtrl.create({
        title:'Deseja realmente EXCLUIR essa atividade?',
        buttons:[
            {
                text:'Sim',
                role: 'confirm',
                handler: (result) => {
                    this.deleteAtiv();
                }
            },
            {
                text:'Não',
                role: 'nop',
                handler: (result) => {
    
                    this.fechar();
                   
                }
            }
        ]

    });
    alert.present();  

  }
  

  fechar(){
    console.log("fechar"+this.data);
    console.log("tipo modal = "+this.tipo);
    let dataReturn = {name:this.name,conteudo:this.conteudo,data:this.data, tipo:this.tipo, concluido:this.concluido}
    this.view.dismiss(dataReturn);
    this.update=false;
  }

}
