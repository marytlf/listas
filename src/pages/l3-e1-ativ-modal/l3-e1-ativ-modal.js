var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController, ViewController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { UsuarioProvider } from '../../providers/usuario/usuario';
/**
 * Generated class for the L3E1AtivModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var L3E1AtivModalPage = /** @class */ (function () {
    function L3E1AtivModalPage(navCtrl, navParams, modalCtrl, toastCtrl, alertCtrl, view, firebase, usuario, alert) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.view = view;
        this.firebase = firebase;
        this.usuario = usuario;
        this.alert = alert;
        this.name = '';
        this.conteudo = '';
        this.tipo = false;
        this.ativ = [];
        this.update = false;
        this.myDate = new Date().toISOString();
        this.date = new Date().toDateString();
        this.name = this.navParams.get("name");
        this.conteudo = this.navParams.get('conteudo');
        this.data = this.navParams.get('data');
        this.tipo = this.navParams.get('tipo');
        this.update = this.navParams.get('update');
        this.id = this.navParams.get('id');
        console.log('aaaaaa' + this.id);
    }
    L3E1AtivModalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad L3E1AtivModalPage');
    };
    L3E1AtivModalPage.prototype.saveAtiv = function () {
        return __awaiter(this, void 0, void 0, function () {
            var janela, e_1, janela;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.firebase.db().collection("atividades").add({
                                name: this.name,
                                data: this.data,
                                conteudo: this.conteudo,
                                tipo: this.tipo,
                            })];
                    case 1:
                        _a.sent();
                        janela = this.toastCtrl.create({
                            message: "Atividade salva com sucesso!",
                            duration: 2000
                        });
                        janela.present();
                        this.fechar();
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        janela = this.toastCtrl.create({
                            message: "Opa! Um erro foi detectado!",
                            duration: 2000
                        });
                        janela.present();
                        throw new Error(e_1);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    L3E1AtivModalPage.prototype.updateAtiv = function () {
        return __awaiter(this, void 0, void 0, function () {
            var janela, dataReturn, e_2, janela;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        if (this.data == null) {
                            this.data = this.myDate;
                        }
                        if (this.tipo == false || this.tipo == null) {
                            this.tipo = false;
                        }
                        return [4 /*yield*/, this.firebase.db().collection("atividades").doc(this.id).update({
                                name: this.name,
                                data: this.data,
                                conteudo: this.conteudo,
                                tipo: this.tipo
                            })];
                    case 1:
                        _a.sent();
                        janela = this.toastCtrl.create({
                            message: "Atividade atualizada com sucesso!",
                            duration: 2000
                        });
                        janela.present();
                        dataReturn = { name: this.name, conteudo: this.conteudo, data: this.data, tipo: this.tipo };
                        this.view.dismiss(dataReturn);
                        this.update = false;
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        janela = this.toastCtrl.create({
                            message: "Opa! Um erro foi detectado!",
                            duration: 2000
                        });
                        janela.present();
                        throw new Error(e_2);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    L3E1AtivModalPage.prototype.deleteAtiv = function () {
        return __awaiter(this, void 0, void 0, function () {
            var janela, e_3, janela;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.firebase.db().collection("atividades").doc(this.id).delete()];
                    case 1:
                        _a.sent();
                        janela = this.toastCtrl.create({
                            message: "Atividade excluída com sucesso!",
                            duration: 2000
                        });
                        janela.present();
                        this.fechar();
                        return [3 /*break*/, 3];
                    case 2:
                        e_3 = _a.sent();
                        janela = this.toastCtrl.create({
                            message: "Opa! Um erro foi detectado!",
                            duration: 2000
                        });
                        janela.present();
                        throw new Error(e_3);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    L3E1AtivModalPage.prototype.presentAlert = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Deseja realmente EXCLUIR essa atividade?',
            buttons: [
                {
                    text: 'Sim',
                    role: 'confirm',
                    handler: function (result) {
                        _this.deleteAtiv();
                    }
                },
                {
                    text: 'Não',
                    role: 'nop',
                    handler: function (result) {
                        _this.fechar();
                    }
                }
            ]
        });
        alert.present();
    };
    L3E1AtivModalPage.prototype.fechar = function () {
        console.log("fechar" + this.data);
        console.log("tipo modal = " + this.tipo);
        var dataReturn = { name: this.name, conteudo: this.conteudo, data: this.data, tipo: this.tipo };
        this.view.dismiss(dataReturn);
        this.update = false;
    };
    L3E1AtivModalPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-l3-e1-ativ-modal',
            templateUrl: 'l3-e1-ativ-modal.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            ModalController,
            ToastController,
            AlertController,
            ViewController,
            FirebaseProvider,
            UsuarioProvider,
            AlertController])
    ], L3E1AtivModalPage);
    return L3E1AtivModalPage;
}());
export { L3E1AtivModalPage };
//# sourceMappingURL=l3-e1-ativ-modal.js.map