import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { L3E1AtivModalPage } from './l3-e1-ativ-modal';

@NgModule({
  declarations: [
    L3E1AtivModalPage,
  ],
  imports: [
    IonicPageModule.forChild(L3E1AtivModalPage),
  ],
})
export class L3E1AtivModalPageModule {}
