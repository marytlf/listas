var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { L3E1AtivModalPage } from './l3-e1-ativ-modal';
var L3E1AtivModalPageModule = /** @class */ (function () {
    function L3E1AtivModalPageModule() {
    }
    L3E1AtivModalPageModule = __decorate([
        NgModule({
            declarations: [
                L3E1AtivModalPage,
            ],
            imports: [
                IonicPageModule.forChild(L3E1AtivModalPage),
            ],
        })
    ], L3E1AtivModalPageModule);
    return L3E1AtivModalPageModule;
}());
export { L3E1AtivModalPageModule };
//# sourceMappingURL=l3-e1-ativ-modal.module.js.map