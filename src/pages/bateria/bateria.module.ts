import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BateriaPage } from './bateria';

@NgModule({
  declarations: [
    BateriaPage,
  ],
  imports: [
    IonicPageModule.forChild(BateriaPage),
  ],
})
export class BateriaPageModule {}
