import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BindPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bind',
  templateUrl: 'bind.html',
})
export class BindPage {

  imagem = '../assets/imgs/4.jpeg';

  public disciplinas = [
    "Programação Web",
    "Programação Móvel",
    "Arquitetura de Sistemas",
    "Frameworks de Desenvolvimento"
  ];


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BindPage');
  }

  back(){
    this.navCtrl.pop();
  }

}
