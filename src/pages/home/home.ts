import { Component } from '@angular/core';
import { NavController, App, ViewController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { UsuarioProvider } from '../../providers/usuario/usuario';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public firebase: FirebaseProvider,
    public usuario: UsuarioProvider,
    public navCtrl: NavController, 
    public appCtrl: App, 
    public viewCtrl : ViewController) {
  }

  back(){
    this.navCtrl.pop();
  }


}
