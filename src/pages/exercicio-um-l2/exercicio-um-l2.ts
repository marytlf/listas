import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, App } from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';

/**
 * Generated class for the ExercicioUmL2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-exercicio-um-l2',
  templateUrl: 'exercicio-um-l2.html',
})
export class ExercicioUmL2Page {

  public divas = [];
  public nome = '';

  public color:string = 'red';
  public background:any = 'blue';
  public bloco:any = true;
  public blocored:any=false;
  public blocoblue:any=false;
  public blocogreen:any=false;

  classes(){
    return {
        bloco: this.bloco,
        blocored:this.blocored,
        blocoblue:this.blocoblue,
        blocogreen:this.blocogreen,
    }
  }

  toggle(colorPar){
     if(colorPar == 'red'){
         return {bloco:this.bloco = false,
                blocoblue:this.blocoblue = false,
                blocogreen:this.blocogreen = false,
                blocored:this.blocored = true}
     }
     if(colorPar == 'blue'){
         return {bloco:this.bloco = false,
                blocored:this.blocored = false,
                blocogreen:this.blocogreen = false,
                blocoblue: this.blocoblue = true}
     }
     if(colorPar == 'green'){
         return {bloco:this.bloco = false,
                blocored:this.blocored = false,
                blocoblue:this.blocoblue = false,
                blocogreen:this.blocogreen = true}
     }
    
  }

  constructor(public firebase: FirebaseProvider,
    public usuario: UsuarioProvider,
    public navCtrl: NavController, 
    public appCtrl: App, 
    public viewCtrl : ViewController) {
      this.carregaDivas();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ExercicioUmL2Page');
  }

  async adicionar(nomeDiva){
    this.nome = nomeDiva;
    await this.firebase.db().collection("divas").add({
      nome: this.nome,
      user_id: this.usuario.get().uid
    });

  }

  async carregaDivas(){
    let results = await this.firebase.db().collection("divas").get();
    this.divas = [];
    results.docs.forEach( doc =>{
      this.divas.push({id: doc.id,...doc.data});
    })
  }

  back(){
    this.navCtrl.pop();
  }



}
