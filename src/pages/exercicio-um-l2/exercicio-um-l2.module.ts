import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExercicioUmL2Page } from './exercicio-um-l2';

@NgModule({
  declarations: [
    ExercicioUmL2Page,
  ],
  imports: [
    IonicPageModule.forChild(ExercicioUmL2Page),
  ],
})
export class ExercicioUmL2PageModule {}
