import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ListaDoisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lista-dois',
  templateUrl: 'lista-dois.html',
})
export class ListaDoisPage {

  public title = 'Titulo da Página';
  public value = '';
  public newName = '';

  mudarTitulo(){
    this.title = this.value;
  }

  novoNome(){
    this.newName =  'Teste';
  }


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListaDoisPage');
  }

  back(){
    this.navCtrl.pop();
  }

}
