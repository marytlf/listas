import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaDoisPage } from './lista-dois';

@NgModule({
  declarations: [
    ListaDoisPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaDoisPage),
  ],
})
export class ListaDoisPageModule {}
