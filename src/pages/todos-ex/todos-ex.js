var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ViewController, ModalController, ToastController, AlertController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { UsuarioProvider } from '../../providers/usuario/usuario';
/**
 * Generated class for the TodosExPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TodosExPage = /** @class */ (function () {
    function TodosExPage(navCtrl, navParams, viewCtrl, appCtrl, modalCtrl, toastCtrl, firebase, alert, usuario) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.appCtrl = appCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.firebase = firebase;
        this.alert = alert;
        this.usuario = usuario;
    }
    TodosExPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TodosExPage');
    };
    TodosExPage.prototype.open = function (paginaPar) {
        this.navCtrl.push(paginaPar);
    };
    TodosExPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-todos-ex',
            templateUrl: 'todos-ex.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            ViewController,
            App,
            ModalController,
            ToastController,
            FirebaseProvider,
            AlertController,
            UsuarioProvider])
    ], TodosExPage);
    return TodosExPage;
}());
export { TodosExPage };
//# sourceMappingURL=todos-ex.js.map