import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TodosExPage } from './todos-ex';

@NgModule({
  declarations: [
    TodosExPage,
  ],
  imports: [
    IonicPageModule.forChild(TodosExPage),
  ],
})
export class TodosExPageModule {}
