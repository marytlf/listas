import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ViewController, ModalController, ToastController, AlertController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { UsuarioProvider } from '../../providers/usuario/usuario';

/**
 * Generated class for the TodosExPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-todos-ex',
  templateUrl: 'todos-ex.html',
})
export class TodosExPage {

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public appCtrl: App,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public firebase: FirebaseProvider,
    public alert: AlertController,
    public usuario: UsuarioProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TodosExPage');
  }

    open(paginaPar){
        this.navCtrl.push(paginaPar);
    }

}
