import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InsertImagePage } from './insert-image';

@NgModule({
  declarations: [
    InsertImagePage,
  ],
  imports: [
    IonicPageModule.forChild(InsertImagePage),
  ],
})
export class InsertImagePageModule {}
