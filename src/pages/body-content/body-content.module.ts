import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BodyContentPage } from './body-content';

@NgModule({
  declarations: [
    BodyContentPage,
  ],
  imports: [
    IonicPageModule.forChild(BodyContentPage),
  ],
})
export class BodyContentPageModule {}
