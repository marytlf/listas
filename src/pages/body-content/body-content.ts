import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the BodyContentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-body-content',
  templateUrl: 'body-content.html',
})
export class BodyContentPage {
    public conteudo:any ='';
    public titulo:any = '';
    public categoria:any = '';

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController) {
        
        this.conteudo = this.navParams.get("conteudo");
        this.titulo = this.navParams.get("titulo");
        this.categoria = this.navParams.get("categoria");
        console.log("construtor = "+this.titulo)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BodyContentPage');
  }
  back(){  
    this.viewCtrl.dismiss();
  }

}
