import { HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import "firebase/firestore";


/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FirebaseProvider {

  constructor(public http: HttpClient) {

    var config = {
      apiKey: "AIzaSyBV9QJSH5Muzw7LPhE90Lx-vrQ-jhaOayo",
      authDomain: "primeiroprojeto-50f65.firebaseapp.com",
      databaseURL: "https://primeiroprojeto-50f65.firebaseio.com",
      projectId: "primeiroprojeto-50f65",
      storageBucket: "primeiroprojeto-50f65.appspot.com",
      messagingSenderId: "890553028418"
    };
    firebase.initializeApp(config);
  }

  db(){
    console.log(firebase.firestore());
    return firebase.firestore();
  }

  auth(){
    console.log(firebase.auth());
    return firebase.auth();
  }

}
