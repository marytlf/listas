var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import "firebase/firestore";
/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var FirebaseProvider = /** @class */ (function () {
    function FirebaseProvider(http) {
        this.http = http;
        var config = {
            apiKey: "AIzaSyBV9QJSH5Muzw7LPhE90Lx-vrQ-jhaOayo",
            authDomain: "primeiroprojeto-50f65.firebaseapp.com",
            databaseURL: "https://primeiroprojeto-50f65.firebaseio.com",
            projectId: "primeiroprojeto-50f65",
            storageBucket: "primeiroprojeto-50f65.appspot.com",
            messagingSenderId: "890553028418"
        };
        firebase.initializeApp(config);
    }
    FirebaseProvider.prototype.db = function () {
        console.log(firebase.firestore());
        return firebase.firestore();
    };
    FirebaseProvider.prototype.auth = function () {
        console.log(firebase.auth());
        return firebase.auth();
    };
    FirebaseProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient])
    ], FirebaseProvider);
    return FirebaseProvider;
}());
export { FirebaseProvider };
//# sourceMappingURL=firebase.js.map