import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

export const atividades = functions.https.onRequest(
    async (request, response) => {
        const ativ = [];
        const results = await admin.firestore().collection("atividades").get();
        results.forEach( doc => {
            ativ.push({
                id: doc.id,
                ...doc.data()
            })
        });
        response.send(ativ);
});


export const upperCaseAtividades = functions.firestore.document('atividades/{atividadeId}')
.onCreate((snap,context) =>{
    const ativ = snap.data();

    ativ.name = ativ.name.toUpperCase();

   
    return admin.firestore().doc('atividades/${context.params.atividadeId}').update(ativ);
});


export const finishedTask = functions.firestore.document('atividades/{atividadeId}')
.onUpdate((snap,context)=>{
    const atividadesresult = snap.before.data();
    const ativConc = [];
    

    if(atividadesresult.concluido === true){
        ativConc.push(atividadesresult)
    }

    
    return admin.firestore().doc('concluido/${context.params.atividadeId}').set(ativConc);
});

export const increment = functions.firestore.document('atividades/{atividadeId}')
.onCreate(async (snap,context)=>{

    const results = await admin.firestore().collection("concluido").doc("qtdConcluido").get();
    const value = results.data();
    value.qtd = value.qtd+1;
    return admin.firestore().doc('concluido/${context.params.qtdConcluido}').update(value);
    
})