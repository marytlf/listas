"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
exports.atividades = functions.https.onRequest((request, response) => __awaiter(this, void 0, void 0, function* () {
    const ativ = [];
    const results = yield admin.firestore().collection("atividades").get();
    results.forEach(doc => {
        ativ.push(Object.assign({ id: doc.id }, doc.data()));
    });
    response.send(ativ);
}));
exports.upperCaseAtividades = functions.firestore.document('atividades/{atividadeId}')
    .onCreate((snap, context) => {
    const ativ = snap.data();
    ativ.name = ativ.name.toUpperCase();
    return admin.firestore().doc('atividades/${context.params.atividadeId}').update(ativ);
});
exports.finishedTask = functions.firestore.document('atividades/{atividadeId}')
    .onUpdate((snap, context) => {
    const atividadesresult = snap.before.data();
    const ativConc = [];
    if (atividadesresult.concluido === true) {
        ativConc.push(atividadesresult);
    }
    return admin.firestore().doc('concluido/${context.params.atividadeId}').set(ativConc);
});
exports.increment = functions.firestore.document('atividades/{atividadeId}')
    .onCreate((snap, context) => __awaiter(this, void 0, void 0, function* () {
    const results = yield admin.firestore().collection("concluido").doc("qtdConcluido").get();
    const value = results.data();
    value.qtd = value.qtd + 1;
    return admin.firestore().doc('concluido/${context.params.qtdConcluido}').update(value);
}));
//# sourceMappingURL=index.js.map